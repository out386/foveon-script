src="src"

function watermark() {
    src_count="$1"
    i="$2"
    src="src"
    dest="dest"
    wm="Foveon.png"
    wm2=$(echo "${3^^}")
    wm3="$4"

    orientation=$(./exiftool -Orientation -n "$i" | awk '{print $3}')
    sfile=$(basename "$i")
    dfile=$dest/$sfile
    dest_count=$(($(ls "$dest" | wc -l)+1));
    
    echo -e "$sfile: $orientation,\t$dest_count/$src_count"

    # None of the values below use density, because that totally messes up the text when the resolution changes
    if [ $orientation -ne 1 ]
    then
        # For potrait images

        convert -auto-orient "$i" "$dfile"
        w=$(identify -format "%w" "$dfile")
        width=$(./bc <<< "0.30*$w")        # Fraction of image width that will be used as the Foveon watermark width
        nameSize=$(./bc <<< "0.016*$w")    # Fraction of image width that will be used as the photographer's watermark width
        nameKern=$(./bc <<< "0.012*$w")    # Photographer's watermark inter-letter spacing
        nameMargins=$(./bc <<< "0.015*$w")
    else
        #for landscape images

        cp "$i" "$dfile"
        w=$(identify -format "%w" "$dfile")
        width=$(./bc <<< "0.20*$w")        # Fraction of image width that will be used as the Foveon watermark width
        nameSize=$(./bc <<< "0.0105*$w")   # Fraction of image width that will be used as the photographer's watermark width
        nameKern=$(./bc <<< "0.0065*$w")   # Photographer's watermark inter-letter spacing
        nameMargins=$(./bc <<< "0.012*$w")
    fi

    if [ -z "$wm3" ]
    then
        # using "-delete 0--1" instead of 3 "+delete"s significantly slows this down
        convert \
            "$dfile" -write mpr:PIC \
            +delete \
            "$wm" -write mpr:WM \
            +delete \
            -respect-parentheses \
                \( mpr:WM -resize $width  +write mpr:WM \) \
                \( -background none -fill "#e5e5e5" -font "Segoe-UI" -pointsize $nameSize -kerning $nameKern label:"$wm2" \
                    -geometry "+$nameMargins+$nameMargins" +write mpr:WM2 \) \
                \( mpr:PIC -gravity NorthWest -quality 100 mpr:WM -composite +write mpr:PIC \) \
                \( mpr:PIC -gravity SouthEast -quality 100 mpr:WM2 -composite +write "$dfile" \) \
        null:
    else
        convert \
            "$dfile" -write mpr:PIC \
            +delete \
            "$wm" -write mpr:WM \
            +delete \
            "$wm3.png" -write mpr:WM3 \
            +delete \
            -respect-parentheses \
                \( mpr:WM -resize $width  +write mpr:WM \) \
                \( -background none -fill "#e5e5e5" -font "Segoe-UI" -pointsize $nameSize -kerning $nameKern label:"$wm2" \
                    -geometry "+$nameMargins+$nameMargins" +write mpr:WM2 \) \
                \( mpr:WM3 -resize $width  +write mpr:WM3 \) \
                \( mpr:PIC -gravity NorthWest -quality 100 mpr:WM -composite +write mpr:PIC \) \
                \( mpr:PIC -gravity SouthEast -quality 100 mpr:WM2 -composite +write mpr:PIC \) \
                \( mpr:PIC -gravity NorthEast -quality 100 mpr:WM3 -composite +write "$dfile" \) \
        null:
    fi
}

export -f watermark
count=$(find "$src" -iname "*.jp*g" -type f | wc -l)

while read -d '' filename; do
    watermark "$count" "${filename}" "$1" "$2" </dev/null
done < <(find "$src" -iname "*.jp*g" -type f -print0)
